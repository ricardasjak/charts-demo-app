// https://plot.ly/javascript/reference/#bar-hoverinfo
type HoverInfoType = 'x' | 'y' | 'x+y' |'all';

// https://plot.ly/javascript/reference/#scatter-mode
type ModeType = 'lines' | 'points' | 'markers' | 'lines+points';

// https://plot.ly/javascript/reference/#scatter
type TraceType = 'scatter';

type XType = string | number;
type YType = string | number;

interface PlotlyChartDataXY {
  x: XType[],
  y: YType[]
}

interface PlotlyChartData extends PlotlyChartDataXY {
  hoverinfo: HoverInfoType,
  mode: ModeType,
  name: string,
  showlegend: boolean,
  type: TraceType
}
