import makePlotlyChartDataXY from "./makePlotlyChartDataXY";

test('makePlotlyChartDataXY', () => {

  const data = [{a: 10, b: '100'}];
  const getX = (dataItem: any) => dataItem.a;
  const getY = (dataItem: any) => dataItem.b;

  const expected: PlotlyChartDataXY = {
    x: [10],
    y: ['100']
  };

  expect(makePlotlyChartDataXY(data, getX,  getY)).toEqual(expected);
});
