type ValueGetter = (entry: any) => string | number;

const makePlotlyChartDataXY = (
  data: any[],
  xValueGetter: ValueGetter,
  yValueGetter: ValueGetter
): PlotlyChartDataXY => {
  return {
    x: data.map(xValueGetter),
    y: data.map(yValueGetter)
  };
};

export default makePlotlyChartDataXY;
