import * as React from 'react';
import Plot from 'react-plotlyjs-ts';
import RangeSlider from "../common/RangeSlider/RangeSlider";
import makePricesPlotlyChartData from "./makePricesPlotlyChartData";

import './PricesChart.css';

interface PricesChartState {
  multiplier: number;
  pricesChartData: PlotlyChartData[];
}

class PricesChart extends React.Component<any, PricesChartState> {

  public state = {
    multiplier: 1,
    pricesChartData: []
  };

  public componentDidMount() {
    this.loadData(this.state.multiplier);
  }

  public render() {
    return (
      <div className="prices-chart">
        <Plot
          data={this.state.pricesChartData}
          layout={{autosize: true, height: 720, title: 'Market data'}}
        />
        <label>
          Multiplier (Math.pow) coefficient
        </label>
        <RangeSlider
          value={this.state.multiplier}
          min={0.1}
          max={3}
          step={0.1}
          onChange={this.handleMultiplierChange}
        />
      </div>
    );
  }

  private handleMultiplierChange = (multiplier: number) => {
    this.loadData(multiplier);
    this.setState({multiplier});
  };

  private loadData = (multiplier: number) => {
    // Normally market data should not be embedded into web app build.
    // Better fetch such data from external server. This is not done, because it's out of demo app scope.
    const marketData = this.getMarketData(multiplier);
    const pricesChartData = makePricesPlotlyChartData(marketData);
    this.setState({pricesChartData});
  };

  // todo: find a way to unit test this "private" fn, make sure there is no mutability in the data source
  private getMarketData(multiplier: number = 1): MarketData[] {
    return (require('../mktdata.json').mktData as MarketData[])
      .map(({instrumentId, timeSeries}) => ({
          instrumentId,
          timeSeries: {
            entries: timeSeries.entries.map(({d, v}) => ({
                d,
                // quick and dirty - I chose Math.pow function to demo more obvious changes in the chart
                v: Math.pow(v, multiplier)
              })
            )
          }
        })
      );
  }
}

export default PricesChart;
