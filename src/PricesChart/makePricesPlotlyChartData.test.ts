import makePricesPlotlyChartData from "./makePricesPlotlyChartData";

test('makePricesPlotlyChartData', () => {
  const marketData: MarketData[] = [{
    instrumentId: 'id-1001',
    timeSeries: {
      entries: [{
        d: 'd1',
        v: 10
      },{
        d: 'd2',
        v: 20
      }]
    },
  }];

  const expected: PlotlyChartData[] = [{
    hoverinfo: 'x+y',
    mode: 'lines',
    name: 'Index-id-1001',
    showlegend: true,
    type: 'scatter',
    x: ['d1', 'd2'],
    y: [10, 20]
  }];

  expect(makePricesPlotlyChartData(marketData)).toEqual(expected);
});
