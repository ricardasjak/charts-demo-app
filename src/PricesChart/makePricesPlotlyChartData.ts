import makePlotlyChartDataXY from "../plotlyData/makePlotlyChartDataXY";

const makePricesPlotlyChartData = (marketData: MarketData[]): PlotlyChartData[] => {
  return marketData.map(({instrumentId, timeSeries}) => {

    const plotlyChartDataDefault: PlotlyChartData = {
        hoverinfo: 'x+y',
        mode: 'lines',
        name: '',
        showlegend: true,
        type: 'scatter',
        x: [],
        y: []
    };
    const plotlyChartXY = makePlotlyChartDataXY(
      timeSeries.entries,
      entry => entry.d,
      entry => entry.v
    );
    return {
      ...plotlyChartDataDefault,
      ...plotlyChartXY,
      name: `Index-${instrumentId}`
    }
  })
};

export default makePricesPlotlyChartData;
