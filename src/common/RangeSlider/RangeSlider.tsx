import * as React from 'react';
// @ts-ignore
import Slider from 'react-rangeslider'
// Import 3rd party styles just for time saving. Normally would never do that.
import 'react-rangeslider/lib/index.css';
import './RangeSlider.css';

interface RangeSliderProps {
  value: number,
  min: number,
  max: number,
  step: number,
  onChange: (value: number) => void
}

const RangeSlider: React.StatelessComponent<RangeSliderProps> = (props) => {

  return (
    <div className="rj-range-slider">
      <Slider
        orientation="horizontal"
        {...props}
      />
      <span className="rj-range-slider-value">
        {/*quick and dirty - need to unit test*/}
        {Math.ceil(props.value * 10) / 10}
      </span>
    </div>
  );
};

export default RangeSlider;
