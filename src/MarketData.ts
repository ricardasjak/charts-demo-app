interface MarketData {
  instrumentId: string,
  timeSeries: {
    entries: Array<{ d: string, v: number }>
  }
}
