import * as React from 'react';
import './App.css';
import PricesChart from "./PricesChart/PricesChart";

const App = () => (
  <div className="app">
    <header>
      <h1>This is charts demo web application</h1>
    </header>
    <PricesChart/>
  </div>
);

export default App;
