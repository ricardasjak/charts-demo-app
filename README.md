## About
This is react demo app to show some chart, created using
[create-react-app](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md),
and [Plotly charts](https://plot.ly/javascript/)

## Start in dev mode
* yarn install
* yarn start
* yarn test (optional)

## Deployment
* yarn build (it creates production-ready bundle in "build" folder)

## Development Notes
* **Why Plotly?** Everyone knows Highcharts or D3 (C3). However I did quick research,
and found some recommendations on the web to try Plotly for some basic charts. I thought,
this is more lightweight library than, for example, Highcharts is, and perfectly
suits given requirements.
* Normally such kind of app could be done without using React, or any other MV* library.
* Create-react-app starter kit was used (along with TS scripts enabled) - it gives
a lot of out of the box (linting, tests setup, ts setup, build command, hot reloading, etc.).
* I really like Typescript. Yes, it slows initial development a little bit.
However it brings great power and control over more complex data structures
and also easies code changes if needed.
* Plotly typescript definitions are lacking. I manually created few ts definitions,
to cover my task needs. I did not have goal to make typescript definition
fully defined. TS definitions also work like mini documentation.
* Main reason to use typescript, is to have better control over data structure
conversion. I also wanted to separate business data (mktdata.json) processing
from purely presentation-driven data model which Plotly chart expects.
* Normally I would add few rendering unit tests, however I got some rendering
error on Plotly component, so I decided to skip this part and not to investigate it.
Furthermore, there is no any conditional rendering logic which might be worth
to unit test.
* `mktdata.json` is embedded into web application bundle, which is not best thing to do.
I would like to host market data on external server and fetch it. I left it as it's
due to demo nature of the app.

##Author
Ricardas Jaksebaga

